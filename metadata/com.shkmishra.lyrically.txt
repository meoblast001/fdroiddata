Categories:Multimedia
License:MIT
Web Site:
Source Code:https://github.com/shkcodes/Lyrically
Issue Tracker:https://github.com/shkcodes/Lyrically/issues

Auto Name:Lyrically
Summary:A lyrics app which works everywhere
Description:
An everywhere lyrics app which can be used by simply swiping a part of your
screen. See the lyrics without interrupting what you're doing.
.

Repo Type:git
Repo:https://github.com/shkcodes/Lyrically

Build:0.11,2
    commit=0.11
    subdir=app
    gradle=yes

Build:0.2,3
    commit=0.2
    subdir=app
    gradle=yes

Build:0.22,6
    commit=0.22
    subdir=app
    gradle=yes

Build:0.3,7
    commit=0.3
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.3
Current Version Code:7
