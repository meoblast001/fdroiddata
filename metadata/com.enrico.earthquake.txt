Categories:Theming
License:GPL-3.0
Web Site:https://github.com/enricocid/Simply-Solid/blob/HEAD/README.md
Source Code:https://github.com/enricocid/Simply-Solid
Issue Tracker:https://github.com/enricocid/Simply-Solid/issues

Auto Name:Simply Solid
Summary:Set solid colors as background
Description:
Pick a solid color as your homescreen background color.
.

Repo Type:git
Repo:https://github.com/enricocid/Simply-Solid

Build:1.0.2,3
    commit=1.0.2
    subdir=project/app
    gradle=yes

Build:2.0.1,9
    commit=2.0.1
    subdir=project/app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.0.1
Current Version Code:9
