AntiFeatures:Tracking
Categories:Multimedia
License:GPL-3.0+
Web Site:https://dulleh.github.io/akhyou/
Source Code:https://github.com/dulleh/akhyou
Issue Tracker:https://github.com/dulleh/akhyou/issues

Auto Name:Akhyou!
Summary:Stream and download anime
Description:
Akhyou! lets you stream or download anime from a variety of streaming sites
using your media player of choice
.

Repo Type:git
Repo:https://github.com/dulleh/akhyou.git

Build:2.0.6,3
    commit=v2.0.6
    subdir=akhyou2
    gradle=fdroid

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.0.6
Current Version Code:3
