Categories:Phone & SMS
License:Apache-2.0
Web Site:
Source Code:https://github.com/kaputnikGo/LittleSirEcho
Issue Tracker:https://github.com/kaputnikGo/LittleSirEcho/issues

Auto Name:Little Sir Echo
Summary:Repeat SMS notifications
Description:
Listens for SMS/MMS package notifications, sets a user-defined timer to remind
using default ringtone and it's own notification message. Has definable awake
times.
.

Repo Type:git
Repo:https://github.com/kaputnikGo/LittleSirEcho

Build:2.1,3
    commit=434e1cba9ded723d090824850daacb7ec202caa1
    target=android-19

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.1
Current Version Code:3
