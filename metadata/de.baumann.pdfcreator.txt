Categories:Writing
License:GPL-3.0+
Web Site:
Source Code:https://github.com/scoute-dich/PDFCreator
Issue Tracker:https://github.com/scoute-dich/PDFCreator/issues
Changelog:https://github.com/scoute-dich/PDFCreator/blob/HEAD/CHANGELOG.md

Auto Name:PDF Creator
Summary:Create and edit PDF files
Description:
Features:

* create PDF-pages from text or pictures
* add PDF-pages from text or pictures to existing PDFs
* merge PDFs
* protect PDFs with password
* delete pages from existing PDFs
* edit metadata from existing PDFs
* convert PDFs to images
.

Repo Type:git
Repo:https://github.com/scoute-dich/PDFCreator.git

Build:1.0.1,2
    commit=v1.0.1
    subdir=app
    gradle=yes

Build:1.0.2,3
    commit=v1.0.2
    subdir=app
    gradle=yes

Build:1.0.3,4
    commit=v1.0.3
    subdir=app
    gradle=yes

Build:1.1,5
    commit=v1.1
    subdir=app
    gradle=yes

Build:2.1,7
    commit=v2.1
    subdir=app
    gradle=yes

Build:2.2,8
    commit=v2.2
    subdir=app
    gradle=yes

Build:2.3,9
    commit=v2.3
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.3
Current Version Code:9
