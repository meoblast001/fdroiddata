Categories:Science & Education
License:MIT
Web Site:
Source Code:https://github.com/gianluca-nitti/android-expr-eval
Issue Tracker:https://github.com/gianluca-nitti/android-expr-eval/issues

Auto Name:ExprEval
Summary:Math expression calculator with variables and functions support
Description:
Application that solves math expressions. Main features:

* Supports variables (e.g. you can do "a=5", then "a+1" and you get 6)
* Built-in basic function (like logarithms and trigonometry)
* Custom functions can be defined (e.g. "log(x,b)=log(x)/log(b)", then log(32,2) will print 5)
* Variables and unctions can be edited both from the prompt and dialogs
* Can print parsing and evaluation steps.

This application is based on [https://github.com/gianluca-nitti/java-expr-eval
this] open source (MIT licensed) library.
.

Repo Type:git
Repo:https://github.com/gianluca-nitti/android-expr-eval.git

Build:1.0,1
    commit=v1.0
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1
